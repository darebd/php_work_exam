<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Task 1 | Lab Exam 1</title>

    <style>
        body {
            width: 100%;
            background-color: white;
            font-family: Arial, sans-serif;
        }
        #container {
            width: 70%;
            margin: 1em auto 0;
        }
    </style>
</head>
<body>

<h2 id="container">
    <!-- Output will be shown here -->
</h2>

<script>
    var givenArray = [23, 6, [2, [6, 2, 1, 2], 2], 5, 2];

    var htmlContent = "";
    var indent = "&nbsp;".repeat(8);
    var displayContainer = document.querySelector("#container");
    displayContainer.innerHTML = "";

    function display(arr, elem, lvl) {

        if (Array.isArray(arr[elem])) {
            display(arr[elem], 0, lvl + 1)
        } else if (arr[elem] === undefined) {
            return htmlContent;
        } else {
            htmlContent += indent.repeat(lvl);
            htmlContent += arr[elem] + "<br />";
        }

        display(arr, elem + 1, lvl);

        return htmlContent
    }

    displayContainer.innerHTML += display(givenArray, 0, 0);
</script>
</body>
</html>

